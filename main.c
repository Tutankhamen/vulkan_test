#include <stdio.h>
#include <stdlib.h>
#include <vulkan/vulkan.h>

#include "clog/clog.h"
int SCOPE_INT_LVL = 0;

void dumpPhysicalDeviceProperties(const VkPhysicalDeviceProperties *physicalDeviceProperties) {
	LOG("name: %s, vendorId: %u, deviceId: %u, type: %u, api: %u, driver: %u", 
			physicalDeviceProperties->deviceName, 
         physicalDeviceProperties->vendorID,
         physicalDeviceProperties->deviceID,
			physicalDeviceProperties->deviceType,
			physicalDeviceProperties->apiVersion,
			physicalDeviceProperties->driverVersion);
}

void dumpQueueFamilyProperties(const VkQueueFamilyProperties *queueFamilyProperties) {
   LOG("flags: 0x%08X, count: %u",
      queueFamilyProperties->queueFlags,
      queueFamilyProperties->queueCount);
}

void dumpPhysicalDeviceMemoryProperties(const VkPhysicalDeviceMemoryProperties *memoryProperties) {
   int midx=0;
   {
		LOG_SCOPE("memoryType");
		for (midx=0; midx<memoryProperties->memoryTypeCount; ++midx) {
		   LOG("%d: %u", midx, memoryProperties->memoryTypes[midx].heapIndex);
	   } 
   }
   {
   	LOG_SCOPE("memoryHeap");
	   for (midx=0; midx<memoryProperties->memoryHeapCount; ++midx) {
		   LOG("%d, %lu ", midx, memoryProperties->memoryHeaps[midx].size>>20);
	   }
   }
}	

int main(int argc, const char* argv) {
	VkInstance instance;
	VkResult result = VK_SUCCESS;
	VkApplicationInfo appInfo = {};
	VkInstanceCreateInfo instanceCreateInfo = {};

   LOG_SCOPE_FUNC;
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "Vulkan Test Application";
	appInfo.applicationVersion = 1;
	appInfo.apiVersion = VK_MAKE_VERSION(1, 0, 0);

	instanceCreateInfo.sType=VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instanceCreateInfo.pApplicationInfo = &appInfo;

	result = vkCreateInstance(&instanceCreateInfo, 0, &instance);
	if (result == VK_SUCCESS) {
		uint32_t physicalDeviceCount = 0;
      uint32_t instanceLayerPropertyCount = 0;
		result = vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, 0);
		if (result == VK_SUCCESS) {
			LOG_SCOPE_ARGS("PhysicalDevices", "%u device(s) were found", physicalDeviceCount);
			if (physicalDeviceCount > 0) {
				VkPhysicalDevice *physicalDevices = malloc(physicalDeviceCount * sizeof(VkPhysicalDevice));
				result = vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, physicalDevices);
				if (result == VK_SUCCESS) {
					uint32_t idx;
               uint32_t queueFamilyPropertiesCount, deviceLayerPropertyCount;
					VkPhysicalDeviceProperties physicalDeviceProperties;
					VkPhysicalDeviceMemoryProperties memoryProperties;
				   for (idx=0; idx<physicalDeviceCount; ++idx) {
	               LOG_SCOPE_ARGS("Device", "idx: %d", idx);
					   // dump device properties	
                  vkGetPhysicalDeviceProperties(physicalDevices[idx], &physicalDeviceProperties);
					   dumpPhysicalDeviceProperties(&physicalDeviceProperties);
						// dump memory properties
                  vkGetPhysicalDeviceMemoryProperties(physicalDevices[idx], &memoryProperties);
                  dumpPhysicalDeviceMemoryProperties(&memoryProperties);
                  // dump queue family properties
                  {
                     vkGetPhysicalDeviceQueueFamilyProperties(physicalDevices[idx], &queueFamilyPropertiesCount, 0);
                     LOG_SCOPE_ARGS("QueueFamilyProperties", "%u properties were found", queueFamilyPropertiesCount);
                     if (queueFamilyPropertiesCount > 0) {
                        uint32_t qidx;
                        VkQueueFamilyProperties *queueFamilyProperties = 
                           malloc(queueFamilyPropertiesCount * sizeof(VkQueueFamilyProperties));
                        vkGetPhysicalDeviceQueueFamilyProperties(
                           physicalDevices[idx], 
                           &queueFamilyPropertiesCount, 
                           queueFamilyProperties);
                        for (qidx=0; qidx<queueFamilyPropertiesCount; ++qidx) {
                           dumpQueueFamilyProperties(&queueFamilyProperties[qidx]);
                        }
                        free(queueFamilyProperties);
                     }
                  }
                  // enumerate device layer properties
                  result = vkEnumerateDeviceLayerProperties(physicalDevices[idx], &deviceLayerPropertyCount, 0);
                  if (result == VK_SUCCESS) {
                     LOG_SCOPE_ARGS("DeviceLayerProperties", "%u properties found", deviceLayerPropertyCount);
                     if (deviceLayerPropertyCount > 0) {
                        uint32_t pidx = 0;
                        VkLayerProperties *deviceLayerProperties = malloc(deviceLayerPropertyCount * sizeof(VkLayerProperties));
                        result = vkEnumerateDeviceLayerProperties(
                           physicalDevices[idx], &deviceLayerPropertyCount, deviceLayerProperties);
                        if (result == VK_SUCCESS) {
                           for (pidx=0; pidx<deviceLayerPropertyCount; ++pidx) {
                              LOG("%u: %s (%s)", idx, deviceLayerProperties[idx].layerName, deviceLayerProperties[idx].description);
                           }
                        } else {
    					         LOG("ERROR: Unable to enumerate device layer properties. Code: %d", result);
	                     }
                        free(deviceLayerProperties);
                     }
                  } else {
   					   LOG("ERROR: Unable to enumerate device layer properties. Code: %d", result);
	               }
				   }
				} else {
					free(physicalDevices);
					LOG("ERROR: Unable to enumerate physical devices. Code: %d", result);
					return -1;
				}

				free(physicalDevices);
			}
		} else {
			LOG("ERROR: Unable to get physical devices count. Code: %d", result);
			return -1;
		}
      // Enumerate instance layer properties
      result = vkEnumerateInstanceLayerProperties(&instanceLayerPropertyCount, 0);
      if (result == VK_SUCCESS) {
         LOG_SCOPE_ARGS("InstanceLayerProperties", "%u properties found", instanceLayerPropertyCount);
         if (instanceLayerPropertyCount > 0) {
            uint32_t idx;
            VkLayerProperties *instanceLayerProperties = malloc(instanceLayerPropertyCount * sizeof(VkLayerProperties));
            result = vkEnumerateInstanceLayerProperties(&instanceLayerPropertyCount, instanceLayerProperties);
            if (result == VK_SUCCESS) {
               for (idx=0; idx<instanceLayerPropertyCount; ++idx) {
                  LOG("%u: %s (%s)", idx, instanceLayerProperties[idx].layerName, instanceLayerProperties[idx].description);
               }
            } else {
      			LOG("ERROR: Unable to enumerate instance layer properties. Code: %d", result);
            }
            free(instanceLayerProperties);
         }
      } else {
			LOG("ERROR: Unable to enumerate instance layer properties. Code: %d", result);
			return -1;
      }
      vkDestroyInstance(instance, 0);
   } else {
		LOG("ERROR: Unable to create instance. Code: %d", result);
		return -1;
	}


	return 0;
}
