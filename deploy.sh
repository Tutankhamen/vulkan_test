#!/bin/bash

SRC=./
DST=root@cheza.lan:/root/

function copy_file() {
	scp -o IdentityFile=${TEST_KEY_LOCATION} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o CheckHostIP=no ${SRC}$1 ${DST}$1
}

copy_file vulkan_test
