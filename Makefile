CC=armv7a-cros-linux-gnueabihf-gcc
CFLAGS=--sysroot=/build/cheza-freedreno
LDFLAGS=-lvulkan

vulkan_test: main.c
	${CC} ${CFLAGS} -o vulkan_test main.c ${LDFLAGS}
